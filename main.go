package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()

	// 添加一个GET接口
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"version": "v0.0.7"})
	})

	// 运行服务
	r.Run(":8080")
}
