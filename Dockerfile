# 基础镜像
FROM registry.cn-hangzhou.aliyuncs.com/godev/golang:1.20 AS build

# 设置工作目录
WORKDIR /app

# 复制项目文件到工作目录
COPY . .
ENV CGO_ENABLED 0
ENV GOOS linux
ENV GOARCH amd64
ENV GOPROXY https://goproxy.cn,direct
ENV GO111MODULE on

# 构建应用
RUN go build -o main .

# 新的镜像阶段
FROM alpine:latest

# 设置工作目录
WORKDIR /app

# 复制编译好的可执行文件到镜像
COPY --from=build /app/main .

# 暴露服务端口
EXPOSE 8080

# 运行应用
CMD ["./main"]
